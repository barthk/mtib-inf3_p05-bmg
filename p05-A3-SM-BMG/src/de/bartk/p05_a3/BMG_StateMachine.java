package de.bartk.p05_a3;

import java.util.Scanner;

public class BMG_StateMachine {

	public static String[] Geraeteausgaben = { "Anzeige: bartK / Willkommen", "Anzeige: bartK / Messung l�uft",
			"Anzeige: bartK / Sys 120 / Dia 80", "Anzeige: bartK / Fehler" };

	public BMG_StateMachine() {

	}

	public enum Ereignisse {
		eONOFF, eSTART, eSUCCESS, eERROR
	}
	
	public static Ereignisse ereignis;
	
	public enum Geraetezustaende {
		eW4_ON, eW4_START, eW4_RES, eW4_CONT
	}
	
	public static Geraetezustaende zustand;

//	public void Ausgaben() {
//		String[] Geraeteausgaben = new String[4];
//		Geraeteausgaben[0] = "Anzeige: bartK / Willkommen";
//		Geraeteausgaben[1] = "Anzeige: bartK / Messung l�uft";
//		Geraeteausgaben[2] = "Anzeige: bartK / Sys 120 / Dia 80";
//		Geraeteausgaben[3] = "Anzeige: bartK / Fehler";

//		StringBuilder ausgabe0 = new StringBuilder();
//		StringBuilder ausgabe1 = new StringBuilder();
//		StringBuilder ausgabe2 = new StringBuilder();
//		StringBuilder ausgabe3 = new StringBuilder();
//
//		ausgabe0.append(Geraeteausgaben[0]);
//		ausgabe1.append(Geraeteausgaben[1]);
//		ausgabe2.append(Geraeteausgaben[2]);
//		ausgabe3.append(Geraeteausgaben[3]);
//	}

	public static Ereignisse EreignisEinlesen() {

		Scanner sc = new Scanner(System.in);
		System.out.print("Eingabe Input Event (eONOFF, eSTART, eSUCCESS, eERROR): ");
		sc.useDelimiter("\\Z");

		String next = sc.nextLine();

		Ereignisse ereignis = null;

		if (next == "eONOFF") {
			ereignis = Ereignisse.eONOFF;
		} else if (next == "eSTART") {
			ereignis = Ereignisse.eSTART;
		} else if (next == "eSUCCESS") {
			ereignis = Ereignisse.eSUCCESS;
		} else if (next == "eERROR") {
			ereignis = Ereignisse.eERROR;
		}

		sc.close();
		return ereignis;

	}

	public static void EreignisAuswerten() {
		
		ereignis = EreignisEinlesen();
		
	switch (zustand) {
		case eW4_ON:
			switch (ereignis) {
			case eONOFF:
				System.out.println(Geraeteausgaben[0].toString());

				zustand = Geraetezustaende.eW4_START;
				break;

			case eSTART:
				System.err.println("falsches Ereignis --> keine Reaktion");
				break;

			case eSUCCESS:
				System.err.println("falsches Ereignis --> keine Reaktion");
				break;

			case eERROR:
				System.err.println("falsches Ereignis --> keine Reaktion");
				break;
			}

		case eW4_START:
			switch (ereignis) {
			case eSTART:
				System.out.println(Geraeteausgaben[1].toString());
				zustand = Geraetezustaende.eW4_RES;
				break;

			case eONOFF:
				zustand = Geraetezustaende.eW4_START;
				break;

			case eSUCCESS:
				System.err.println("falsches Ereignis --> keine Reaktion");
				break;
			case eERROR:
				System.err.println("falsches Ereignis --> keine Reaktion");
				break;
			}

		case eW4_RES:
			switch (ereignis) {
			case eSTART:
				System.out.println(Geraeteausgaben[1].toString());
				zustand = Geraetezustaende.eW4_RES;
				break;

			case eSUCCESS:
				System.out.println(Geraeteausgaben[2].toString());
				zustand = Geraetezustaende.eW4_CONT;
				break;

			case eERROR:
				System.out.println(Geraeteausgaben[3].toString());
				zustand = Geraetezustaende.eW4_CONT;
				break;

			case eONOFF:
				zustand = Geraetezustaende.eW4_START;
				break;
			}

		case eW4_CONT:
			switch (ereignis) {
			case eSTART:
				System.out.println(Geraeteausgaben[1].toString());
				zustand = Geraetezustaende.eW4_RES;
				break;

			case eONOFF:
				zustand = Geraetezustaende.eW4_START;
				break;

			case eSUCCESS:
				System.err.println("falsches Ereignis --> keine Reaktion");
				break;

			case eERROR:
				System.err.println("falsches Ereignis --> keine Reaktion");
				break;
			}
		}
	}

	public static void main(String[] args) {
		EreignisEinlesen();
		EreignisAuswerten();

//		while (true) {
//
//			EreignisEinlesen(null);
//
//			EreignisAuswerten(Eingang.eONOFF, Geraetezustaende.eW4_START);
//
//		}

	}

}
